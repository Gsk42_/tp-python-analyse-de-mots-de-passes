import random, string, secrets, re, sys

import datetime


filename = "logs/log.txt"
mode = "r"

def check_password(str):
    if len(str) < 10:
        sys.stderr.write("le mot de passe doit contenir au moins 10 caractères\n")
        return False
    # le mot de passe doit contenir au moins 2 digits
    digit_count = 0
    non_alpha_count = 0
    for i in str:
        if i.isdigit():
            digit_count += 1
    if digit_count < 2:
        sys.stderr.write("le mot de passe doit contenir au moins 2 digits\n")
        return False
    # le mot de passe doit contenir au moins 2 digits
    for i in str:
        if i.isalnum():
            non_alpha_count += 1
    # le mot de passe doit contenir au moins 1 caractère spécial
    if non_alpha_count < 1:
        sys.stderr.write("le mot de passe doit contenir au moins 1 caractère spécial\n")
        return False
    else:
        return True
    

def contain_exception(str):
    for i in exceptions:
        if re.search('(.+)'+i, str) != None:
            sys.stderr.write("Le mot de passe ne doit pas contenir les séquences suivantes: "+i+"\n")
            return False
        if  re.search(i+'(.+)', str) != None:
            sys.stderr.write("le mot de passe ne doit pas contenir les séquences suivantes: "+i+"\n")
            return False
    return True

def is_repeated(s):
    regex = r'(.)\1+'
    match = re.search(regex, s)
    if match != None:
        sys.stderr.write("le mot de passe ne doit pas contenir de caractère répété\n")
        return False
    return True

def is_palindrome(s):
    return s == s[::-1]

# fonction qui génère un mot de passe aléatoire
def generate_pass():
    
    length = random.randint(10, 20)
    return ''.join(secrets.choice(string.ascii_letters + string.digits + string.hexdigits + string.punctuation) for _ in range(length))


#fonction pour ecrire dans un fichier a une ligne donnée

def write_file(filename, msg):
    fileobj = open(filename, "r+") 
    fileobj.readlines()
    
    fileobj.write(msg + "\n")
    


def generate_valid_pass(nb):
    
    # on récupère la date actuelle
    date = datetime.datetime.now()
    i = 0
    passwords = []
    
    # on génère nb mots de passe
    while i < nb:
        password = generate_pass()
        
        # si le mot de passe est valide on l'ajoute à la liste
        if check_password(password):
            i += 1
            passwords.append(password)
        # sinon on l'écrit dans le fichier log.txt
        else:
            msg  = password
            print("Le mot de passe n'est pas valide" + "\n" + password)
            write_file(filename, msg)
            
    # on affiche la liste des mots de passe générés
    print(passwords)



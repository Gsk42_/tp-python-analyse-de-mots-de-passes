import re, sys

"""
Module check_mdp
Auteur : Yoan Kpatchavi

Ce module fournit des fonctions pour vérifier la validité des mots de passe. Les règles de validation incluent 
la longueur minimale du mot de passe, la présence de chiffres et de caractères spéciaux et l'absence de certaines 
chaînes de caractères spécifiques.

Fonctions :
- check_password : Vérifie la longueur du mot de passe, la présence de chiffres et de caractères spéciaux.
- contain_exception : Vérifie que le mot de passe ne contient pas certaines chaînes de caractères spécifiques.
- is_repeated : Vérifie qu'aucun caractère n'est répété dans le mot de passe.
- is_palindrome : Vérifie si une chaîne de caractères est un palindrome.
- verif : Effectue tous les checks sur le mot de passe.
"""
#toutes ces fonctions doivent retourner True si le mot de passe est valide, False sinon

exceptions = ["azer", "Azer", "AzEr", "aZeR", "AZER", "qwer", "QWER", "123", "321"]

def check_password(str):
    if len(str) < 10:
        sys.stderr.write("le mot de passe doit contenir au moins 10 caractères\n")
        return False
    # le mot de passe doit contenir au moins 2 digits
    digit_count = 0
    non_alpha_count = 0
    for i in str:
        if i.isdigit():
            digit_count += 1
    if digit_count < 2:
        sys.stderr.write("le mot de passe doit contenir au moins 2 digits\n")
        return False
    # le mot de passe doit contenir au moins 2 digits
    for i in str:
        if i.isalnum():
            non_alpha_count += 1
    # le mot de passe doit contenir au moins 1 caractère spécial
    if non_alpha_count < 1:
        sys.stderr.write("le mot de passe doit contenir au moins 1 caractère spécial\n")
        return False
    else:
        return True
    

def contain_exception(str):
    for i in exceptions:
        if re.search('(.+)'+i, str) != None:
            sys.stderr.write("Le mot de passe ne doit pas contenir les séquences suivantes: "+i+"\n")
            return False
        if  re.search(i+'(.+)', str) != None:
            sys.stderr.write("le mot de passe ne doit pas contenir les séquences suivantes: "+i+"\n")
            return False
    return True

def is_repeated(s):
    regex = r'(.)\1+'
    match = re.search(regex, s)
    if match != None:
        sys.stderr.write("le mot de passe ne doit pas contenir de caractère répété\n")
        return False
    return True

def is_palindrome(s):
    return s == s[::-1]

CHECKS = [check_password, contain_exception, is_repeated, is_repeated]

def verif(password):
    for check in CHECKS:
        if not check(password):
            return False
    return True

import utilis.check_mdp as cm
import utilis.generate_mdp as gm


def main():
    """
    Programme principal
    Auteur : Yoan Kpatchavi

    Ce programme permet à un utilisateur de générer et d'analyser des mots de passe selon des critères spécifiques.
    Il offre une interface en ligne de commande avec un menu pour choisir entre la génération de mots de passe, 
    l'analyse de mots de passe et la sortie du programme.

    Fonctions :
    - main : La fonction principale qui gère le menu et les appels aux autres fonctions.
    """    
    print ("#############################################################################")
    print( "########                                                             ########")
    print( "########               My Company Super Client Number 1'             ########")
    print( "######## ----------------------------------------------------------- ########")
    print( "######## Customer relationship management for Super Client Number 1  ########")
    print ("######## ----------------------------------------------------------- ########")
    print( "########                                                             ########")
    print( "########                    Choose a option:                         ########")
    print( "########                                                             ########")
    print( "#############################################################################")
    print( "########      1) Générer un mot de passe | 2) Analyser un mot de passe ######")
    print( "########      3) Sortir                                               #######")
    print( "########                                                             ########")
        
    
        
    choix = input("########      Votre choix: ")
        
    if choix.isdigit() == False:
            print("########      Choix invalide")
            print("########      --------------")
            return 0
        
    if int(choix) == 1:
            print("########      Générer un mot de passe")
            print("########      ----------------------")
            return 1
            
    elif int(choix) == 2:
            print("########      Analyser un mot de passe")
            print("########      ----------------------")
            return 2
            
    elif int(choix) == 3:
            print("########      Sortir")
            print("########      ------")
            return 3
if __name__ == "__main__": 
    
    while True:
        result = main()
        if result == 3:
            break
        elif result == 2:
            pwd = input("########      Entrez le mot de passe: ")
            result_pwd = cm.verif(pwd)
            
            if result_pwd == True:
                print("########      Le mot de passe est valide")
                break
            else :
                print("########      Le mot de passe n'est pas valide")
                print("########      ",result_pwd, "\n" * 10)
        
        elif result == 1:
            nb_pass = input("########      Entrez le nombre de mot de passe à générer: ")
            
            if nb_pass.isdigit() == False:
                print("########      Choix invalide")
                print("########      --------------")
                break
            else:
                nb_pass = int(nb_pass)
                gm.generate_valid_pass(nb_pass)
                break
            
                